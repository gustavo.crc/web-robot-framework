*** Settings ***
Documentation    Este é um teste dedicado para a funcionalidade de Login
Resource         ../resourses/base.robot

Test Setup       Abrir Navegador
Test Teardown    Fechar Navegador

*** Variables ***
${MSG_ERRO}    Epic sadface: Username and password do not match any user in this service

# Rodar com tag: 
# robot -d results -i LoginSuccess tests
*** Test Cases ***
Validar login com sucesso no site
    [Tags]                         Login    LoginSucesso
    Realizar Login                 standard_user    secret_sauce
    Validar direcionamento pra home

Validar login com usuario invalido
    [Tags]                         Login    LoginInvalido
    Realizar Login                                    invalid_user    inmetrics
    Validar mensagem de erro na tentativa de Login    ${MSG_ERRO}
*** Settings ***
Documentation    Este é um teste dedicado para o fluxo de checkout
Library          FakerLibrary    locale=pt_BR
Resource         ../resourses/base.robot

Test Setup       Abrir Navegador
Test Teardown    Fechar Navegador

*** Variables ***
${FILTRO_Z_TO_A}    Name (Z to A)
${PRODUTO}          Sauce Labs Bike Light

*** Test Cases ***
Validar o fluxo de checkout com sucesso
    [Tags]    Checkout    CheckoutCompleto
    Realizar Login                          standard_user    secret_sauce
    Adicionar Produto no Carrinho           ${FILTRO_Z_TO_A}    ${PRODUTO} 
    Clicar no Icone do Carrinho
    Validar que o Produto esteja Visivel    ${PRODUTO} 
    Clicar no Botao Checkout
    ${NOME}         FakerLibrary.first_name
    ${SOBRENOME}    FakerLibrary.last_name
    ${CEP}          FakerLibrary.postcode
    Preencher Formulario no Checkout        ${NOME}    ${SOBRENOME}    ${CEP}
    Clicar no Botao Continuar
    Validar que o Produto esteja Visivel    ${PRODUTO}
    Clicar no Botao Finish
    Validar Pedido Finalizado com Sucesso

Validar tentativa de checkout sem preencher o formulario
    [Tags]    Checkout    CheckoutFormVazio
    Realizar Login                          standard_user    secret_sauce
    Adicionar Produto no Carrinho           ${FILTRO_Z_TO_A}    ${PRODUTO} 
    Clicar no Icone do Carrinho
    Validar que o Produto esteja Visivel    ${PRODUTO} 
    Clicar no Botao Checkout
    Clicar no Botao Continuar
    Validar Mensagem de Erro
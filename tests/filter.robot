*** Settings ***
Documentation    Este é um teste dedicado para a funcionalidade no filtro da home do site
Resource         ../resourses/base.robot

Test Setup       Abrir Navegador
Test Teardown    Fechar Navegador

*** Variables ***
${OPCAO_FILTRO}      class=product_sort_container
${PRECO}             css=.inventory_item:nth-of-type(1) .inventory_item_price
${ROUPA_INFANTIL}    css=.inventory_item:nth-of-type(1) #item_2_title_link
${BLUSA_ZIPER}       css=.inventory_item:nth-of-type(1) #item_5_title_link

*** Test Cases ***
Validar o filtro de menor preço para o maior
    [Tags]                         Filtro    FiltroLowToHigh
    Realizar Login                 standard_user    secret_sauce

    Select From List By Label      ${OPCAO_FILTRO}      Price (low to high)
    Element Should Contain         ${ROUPA_INFANTIL}    Sauce Labs Onesie
    Element Should Contain         ${PRECO}             $7.99

Validar o filtro de maior preço para o menor
    [Tags]                         Filtro    FiltroHighToLow
    Realizar Login                 standard_user    secret_sauce

    Select From List By Label      ${OPCAO_FILTRO}    Price (high to low)
    Element Should Contain         ${BLUSA_ZIPER}     Sauce Labs Fleece Jacket
    Element Should Contain         ${PRECO}           $49.99
*** Settings ***
Resource    ../base.robot

*** Keywords ***
Realizar Login
    [Arguments]      ${USUARIO}    ${SENHA}
    Input Text       id=user-name    ${USUARIO}
    Input Text       id=password     ${SENHA}
    Click Element    id=login-button

Validar direcionamento pra home
    Page Should Contain Element    class=shopping_cart_link 
    Element Should Contain         class=title    PRODUCTS

Validar mensagem de erro na tentativa de Login
    [Arguments]               ${MENSAGEM}
    Element Should Contain    class=error-message-container    ${MENSAGEM}
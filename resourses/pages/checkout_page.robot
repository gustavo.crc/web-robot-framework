*** Settings ***
Resource    ../base.robot

*** Keywords ***
Adicionar Produto no Carrinho
    [Arguments]                    ${FILTRO}    ${NOME_PRODUTO}
    Select From List By Label      class=product_sort_container    ${FILTRO}
    Click Element                  xpath=//*[div="${NOME_PRODUTO}"]
    Click Button                   Add to cart

Clicar no Icone do Carrinho
    Click Element    class=shopping_cart_link

Validar que o Produto esteja Visivel
    [Arguments]            ${PRODUTO}
    Page Should Contain    ${PRODUTO}

Preencher Formulario no Checkout
    [Arguments]      ${PRIMEIRO_NOME}    ${ULTIMO_NOME}    ${CEP}
    Input Text       id=first-name       ${PRIMEIRO_NOME}
    Input Text       id=last-name        ${ULTIMO_NOME}
    Input Text       id=postal-code      ${CEP}

Clicar no Botao Continuar
    Click Element    id=continue

Clicar no Botao Checkout
    Wait Until Element Is Visible    id=checkout    5s
    Click Element                    id=checkout

Clicar no Botao Finish
    Wait Until Element Is Visible    id=finish    5s
    Click Element                    id=finish

Validar Pedido Finalizado com Sucesso
    Wait Until Element Is Visible    class=complete-header    5s
    Element Should Contain           class=complete-header    THANK YOU FOR YOUR ORDER
    Capture Page Screenshot          Pedido Concluido.png

Validar Mensagem de Erro
    Element Should Contain           class=error-message-container    Error: First Name is required
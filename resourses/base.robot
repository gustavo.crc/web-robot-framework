*** Settings ***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ./pages/login_page.robot
Resource    ./pages/checkout_page.robot

*** Variables ***
${NAVEGADOR_CHROME}    chrome
${URL}                 https://www.saucedemo.com/ 
${PATH_SCREENSHOT}     ./results

*** Keywords ***
Abrir Navegador
    Set Selenium Timeout        5
    Remove files                ${PATH_SCREENSHOT}/*.png
    Open Browser                ${URL}    ${NAVEGADOR_CHROME}
    Set Screenshot Directory    ${PATH_SCREENSHOT}
    Maximize Browser Window

Fechar Navegador
    Capture Page Screenshot     ${TEST NAME}.png
    Close All Browsers